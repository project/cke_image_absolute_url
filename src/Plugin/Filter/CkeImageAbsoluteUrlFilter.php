<?php

namespace Drupal\abs_image_url\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;

/**
 * Provides a filter to track images uploaded via a Text Editor and exposing it as absoulte URL.
 *
 * @Filter(
 *   id = "cke_image_absolute_url",
 *   title = @Translation("Track images uploaded via a Text Editor with absolut path (Place this filter at the end of the 'Filter processing order')"),
 *   description = @Translation("Ensures that the latest versions of images uploaded via a Text Editor are displayed with absolute path."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class EditorFileReferenceAbsUrl extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $body = Markup::create(Html::transformRootRelativeUrlsToAbsolute($text, $GLOBALS['base_url']));
    $result->setProcessedText($body);
    return $result;
  }

    public function tips($long = FALSE) {
    return $this->t("Please make sure this filter placed at the end in 'processing filter order'");
  }
}